#!/usr/bin/env python3

import os
import pytest

run_all = os.getenv('PYTEST_RUN_ALL', 'F').upper() \
    in ('TRUE', 'T', 'YES', 'Y')

if run_all:
    from ...Test.API.pure import *

else:    
    from ...Test.API.pure import \
        test_ping as __test_ping

