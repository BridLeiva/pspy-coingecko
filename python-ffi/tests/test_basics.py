#!/usr/bin/env python3

import os
import pytest

from ... import *

run_all = os.getenv('PYTEST_RUN_ALL', 'F').upper() \
    in ('TRUE', 'T', 'YES', 'Y')

@pytest.mark.skipif(not run_all, reason= "avoid making too many calls")
def test_ping():
    response = ping_api()
    assert response['gecko_says'] == '(V3) To the Moon!'

@pytest.mark.skipif(not run_all, reason= "avoid making too many calls")
def test_get_price():
    # simple price
    response = get_price(ids= "bitcoin", vs_currencies= "usd")

    assert response, "Must have a response"
    assert 'bitcoin' in response \
        and 'usd' in response['bitcoin'], \
        "must have the correct structure"


    # several coins
    coin_ids = ['bitcoin', 'litecoin', 'ethereum']
    vs_currencies = ['usd', 'eur']
    
    response = get_price(
        ids= coin_ids, vs_currencies= vs_currencies
    )

    assert response, "Must have a response"
    assert all([
        coin in response \
            and all(curr in response[coin] for curr in vs_currencies)
        for coin in coin_ids
    ]), "must have the correct structure"

    
@pytest.mark.skipif(not run_all, reason= "avoid making too many calls")
def test_get_coins():
    # listing coins
    response = get_coins_list()

    assert response, "Must have a response"
    assert all([
        'id' in cur \
            and 'name' in cur \
            and 'symbol' in cur \
        for cur in response
    ])
