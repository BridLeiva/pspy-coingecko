#!/usr/bin/env python3

import os
import pytest

from ... import *

run_all = os.getenv('PYTEST_RUN_ALL', 'F').upper() \
    in ('TRUE', 'T', 'YES', 'Y')

def test_roiOne():
    assert roiOne(100)(110) == 10

def test_roiList():
    assert roiList(())([10, 20]) == ()
    assert roiList([10, 20])(()) == ()
    
    
    investments = (10, 20, 30, 40)
    values      = (23, 15, 45, 23)
    assert roiList(investments)(values) \
        == (130.0, -25.0, 50.0, -42.5)

    