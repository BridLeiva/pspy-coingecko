from pycoingecko import CoinGeckoAPI

cg = CoinGeckoAPI()

def ping_api():
    return cg.ping()

def get_price(**api_call_args):
    return cg.get_price(**api_call_args)

#exposing the rest of functions from pycoingecko
funct_list = [
    'get_token_price'
    , 'get_supported_vs_currencies'
    , 'get_coins_list'
    , 'get_coins_markets'
    , 'get_coin_by_id'
    , 'get_coin_ticker_by_id'
    , 'get_coin_history_by_id'
    , 'get_coin_market_chart_by_id'
    , 'get_coin_market_chart_range_by_id'
    , 'get_coin_status_updates_by_id'
    , 'get_coin_ohlc_by_id'
    , 'get_coin_info_from_contract_address_by_id'
    , 'get_coin_market_chart_from_contract_address_by_id'
    , 'get_coin_market_chart_range_from_contract_address_by_id'
    , 'get_asset_platforms'
    , 'get_coins_categories_list'
    , 'get_coins_categories'
    , 'get_exchanges_list'
    , 'get_exchanges_id_name_list'
    , 'get_exchanges_by_id'
    , 'get_exchanges_tickers_by_id'
    , 'get_exchanges_status_updates_by_id'
    , 'get_exchanges_volume_chart_by_id'
    , 'get_finance_platforms'
    , 'get_finance_products'
    , 'get_indexes'
    , 'get_indexes_by_market_id_and_index_id'
    , 'get_indexes_list'
    , 'get_derivatives'
    , 'get_derivatives_exchanges'
    , 'get_derivatives_exchanges_by_id'
    , 'get_derivatives_exchanges_list'
    , 'get_status_updates'
    , 'get_events'
    , 'get_events_countries'
    , 'get_events_types'
    , 'get_exchange_rates'
    , 'get_search_trending'
    , 'get_global'
    , 'get_global_decentralized_finance_defi'
    , 'get_companies_public_treasury_by_coin_id' 
]

for funct in funct_list:
    eval(f"({funct} := lambda **call_args: cg.{funct}(**call_args))")


