#!/usr/bin/env python3

from importlib import import_module
from inspect import signature

from .API.pure import funct_list
from .API.pure import ping_api
from .API.pure import get_price 
    
mod = import_module('.API.pure', 'pspygecko')
for f in funct_list: eval(f"({f} := getattr(mod, '{f}'))")

from .Metrics.pure import roiOne, roiList
