module Main where

import Prelude

import Effect (Effect)
import Effect.Console (log)

-- importing other modules so pspy builds them
import API
import Metrics
import Test.API


main :: Effect Unit
main = do
  log "🍝"
  response <- ping_api
  log $ show response.gecko_says
