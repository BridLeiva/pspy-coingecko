module Metrics where

import Prelude
import Data.Array
-- import Data.List

import API (get_price)

-- model
type Investment = Number
type Value = Number
type ReturnPercentage = Number


type RoiOneArgs = {
    coin :: String
    , vs_currencies :: String
    , investment_capital :: Investment
}

type RoiListArgs = {
    coin :: Array String
    , vs_currencies :: Array String
    , investment_capital :: Array Investment
}

data RoiArgs = RoiOneArgs | RoiListArgs
data RoiResponse = ReturnPercentage | Array ReturnPercentage


-- functions
roiOne :: Investment -> Value -> ReturnPercentage
roiOne inv val = 100.0 * (val - inv) / inv 

roiList :: Array Investment -> Array Value -> Array ReturnPercentage
roiList invs vals = case null invs of
    true -> []
    false -> case null vals of
        true -> []
        false -> zipWith roiOne invs vals

-- roi :: RoiArgs -> Effect RoiResponse
-- roi {coin: c, vs_currencies: curr, investment_capital: inv} = case coin of
--     String -> do 
--         response <- get_price { ids: c, vs_currencies: curr }
--         roiOne inv response.