module API where

import Prelude
import Effect (Effect)


data ParamList = String | Array String

type ApiCallArgs = {
  ids :: ParamList
  , vs_currencies :: ParamList
}

type PingResponse = { gecko_says :: String }
type ApiResponse = {}


foreign import ping_api :: Effect PingResponse
foreign import funct_list :: Array String

foreign import get_price :: ApiCallArgs -> Effect ApiResponse

foreign import get_token_price :: ApiCallArgs -> Effect ApiResponse
foreign import get_supported_vs_currencies :: ApiCallArgs -> Effect ApiResponse
foreign import get_coins_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_coins_markets :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_ticker_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_history_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_market_chart_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_market_chart_range_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_status_updates_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_ohlc_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_info_from_contract_address_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_market_chart_from_contract_address_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_coin_market_chart_range_from_contract_address_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_asset_platforms :: ApiCallArgs -> Effect ApiResponse
foreign import get_coins_categories_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_coins_categories :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_id_name_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_tickers_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_status_updates_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchanges_volume_chart_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_finance_platforms :: ApiCallArgs -> Effect ApiResponse
foreign import get_finance_products :: ApiCallArgs -> Effect ApiResponse
foreign import get_indexes :: ApiCallArgs -> Effect ApiResponse
foreign import get_indexes_by_market_id_and_index_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_indexes_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_derivatives :: ApiCallArgs -> Effect ApiResponse
foreign import get_derivatives_exchanges :: ApiCallArgs -> Effect ApiResponse
foreign import get_derivatives_exchanges_by_id :: ApiCallArgs -> Effect ApiResponse
foreign import get_derivatives_exchanges_list :: ApiCallArgs -> Effect ApiResponse
foreign import get_status_updates :: ApiCallArgs -> Effect ApiResponse
foreign import get_events :: ApiCallArgs -> Effect ApiResponse
foreign import get_events_countries :: ApiCallArgs -> Effect ApiResponse
foreign import get_events_types :: ApiCallArgs -> Effect ApiResponse
foreign import get_exchange_rates :: ApiCallArgs -> Effect ApiResponse
foreign import get_search_trending :: ApiCallArgs -> Effect ApiResponse
foreign import get_global :: ApiCallArgs -> Effect ApiResponse
foreign import get_global_decentralized_finance_defi :: ApiCallArgs -> Effect ApiResponse
foreign import get_companies_public_treasury_by_coin_id :: ApiCallArgs -> Effect ApiResponse
