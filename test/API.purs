module Test.API where

import Prelude

import Effect (Effect)
import Effect.Class.Console (log)

import Test.Assert (assertEqual)

import API (ping_api)

main :: Effect Unit
main = do
    log "🍝"
    log "You should add some tests."

test_ping :: Effect Unit
test_ping = do
    response <- ping_api
    log $ show response.gecko_says

    assertEqual { actual: response.gecko_says, expected: "(V3) To the Moon!" }